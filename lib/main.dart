// import 'package:componentes/src/pages/home_temp.dart';
import 'package:componentes/src/pages/home_page.dart'; // lab 13
import 'package:flutter/material.dart';

import 'package:componentes/src/pages/alert_page.dart';
import 'package:componentes/src/pages/avatar_page.dart';
import 'package:componentes/src/pages/cards_page.dart';

import 'package:componentes/src/pages/animated_container_page.dart';
import 'package:componentes/src/pages/inputs_page.dart';
import 'package:componentes/src/pages/listas_page.dart';
import 'package:componentes/src/pages/slider_checks_page.dart';



void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      // home: HomePageTemp(),
      // home: HomePage(), // lab 13
      initialRoute: '/',
      routes:{
        '/' : (context) => HomePage(),
        'alert': (context) => AlertPage(),
        'avatar': (context) => AvatarPage(),
        'cards': (context) => CardsPage(),
        
        'animated': (context) => AnimatedContainerPage(),
        'inputs': (context) => InputsPage(),
        'slider': (context) => SliderPage(),
        'listas': (context) => ListasPage(),




      }
    );
  }
}
