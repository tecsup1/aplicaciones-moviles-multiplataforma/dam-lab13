import 'package:flutter/material.dart';

final _icons = <String, IconData>{
  'add_alert': Icons.add_alert,
  'accessibility': Icons.accessibility,
  'folder_open': Icons.folder_open,

  'business_center': Icons.business_center,
  'input': Icons.input,
  'playlist_add_check': Icons.playlist_add_check,
  'list': Icons.list,
};

Icon getIcon(String nombreIcono) {
  return Icon(_icons[nombreIcono]);
}
