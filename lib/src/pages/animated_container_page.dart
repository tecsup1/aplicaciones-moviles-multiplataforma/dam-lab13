import 'package:flutter/material.dart';

class AnimatedContainerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animated container page'),
        backgroundColor: Colors.black,
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        // mainAxisSize: MainAxisSize.max,
        // mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            "Estás en la página de contenedores animados",
            textAlign: TextAlign.center,
            style: TextStyle(
              height: 2,
              fontSize: 20,
              color: Colors.red,
            ),
          ),
          Image.network(
            'https://ak.picdn.net/shutterstock/videos/19762039/thumb/1.jpg',
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        onPressed: () => {Navigator.pop(context)},
        backgroundColor: Colors.black,
      ),
    );
  }
}
