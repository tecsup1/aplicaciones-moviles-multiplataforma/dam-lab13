import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alert page'),
        backgroundColor: Colors.black,
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        // mainAxisSize: MainAxisSize.max,
        // mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            "Estás en la página de alertas",
            textAlign: TextAlign.center,
            style: TextStyle(
              height: 2,
              fontSize: 20,
              color: Colors.red,
            ),
          ),
          Image.network(
            'https://as.com/meristation/imagenes/2015/09/16/noticia/1442395140_772736_1532446572_sumario_normal.jpg',
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        onPressed: () => {Navigator.pop(context)},
        backgroundColor: Colors.black,
      ),
    );
  }
}
