import 'package:flutter/material.dart';

class InputsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('inputs page'),
        backgroundColor: Colors.black,
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        // mainAxisSize: MainAxisSize.max,
        // mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            "Estás en la página de inputs",
            textAlign: TextAlign.center,
            style: TextStyle(
              height: 2,
              fontSize: 20,
              color: Colors.red,
            ),
          ),
          Image.network(
            'https://wallpaper-mania.com/wp-content/uploads/2018/09/High_resolution_wallpaper_background_ID_77701145599.jpg',
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        onPressed: () => {Navigator.pop(context)},
        backgroundColor: Colors.black,
      ),
    );
  }
}
