import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar page'),
        backgroundColor: Colors.black,
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        // mainAxisSize: MainAxisSize.max,
        // mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            "Estás en la página de avatar",
            textAlign: TextAlign.center,
            style: TextStyle(
              height: 2,
              fontSize: 20,
              color: Colors.red,
            ),
          ),
          Image.network(
            'https://hipertextual.com/files/2020/01/hipertextual-arte-conceptual-avatar-2-muestra-nuevos-rincones-pandora-2020623395.jpg',
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        onPressed: () => {Navigator.pop(context)},
        backgroundColor: Colors.black,
      ),
    );
  }
}
