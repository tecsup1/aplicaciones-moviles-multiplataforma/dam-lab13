import 'package:flutter/material.dart';

class SliderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Slider page'),
        backgroundColor: Colors.black,
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        // mainAxisSize: MainAxisSize.max,
        // mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            "Estás en la página de sliders",
            textAlign: TextAlign.center,
            style: TextStyle(
              height: 2,
              fontSize: 20,
              color: Colors.red,
            ),
          ),
          Image.network(
            'https://2.bp.blogspot.com/-uaMeWjMsGAw/VGn8_HuEjsI/AAAAAAAAHQ8/kquSrQwSZm8/s1600/interstellar-movie-2014-hd-wallpapers-full.jpg',
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        onPressed: () => {Navigator.pop(context)},
        backgroundColor: Colors.black,
      ),
    );
  }
}
